from django.core.management.base import BaseCommand

from app.internal import bot


class Command(BaseCommand):
	help = 'Starts telegram bot from app/internal/bot.py'

	def handle(self, *args, **kwargs):
		print('bot start to work')
		bot.run()
