from dataclasses import dataclass, field

from telegram.ext import CommandHandler, ConversationHandler, Updater

from app.internal.transport.bot import handlers
from config.settings import TG_BOT_TOKEN


@dataclass
class TgBot:
    _token: str
    _updater: Updater = field(init=False)

    def __post_init__(self):
        self._updater = Updater(self._token)
        entry_points = [
            CommandHandler("start", handlers.start_handler),
            CommandHandler("me", handlers.me_handler),
            CommandHandler("set_phone", handlers.set_phone_handler),
        ]

        conversation_handler = ConversationHandler(
            entry_points=entry_points, allow_reentry=True, states={}, fallbacks=[]
        )
        self._updater.dispatcher.add_handler(conversation_handler)

    def run(self):
        try:
            self._updater.start_polling()
        except:
            pass


def run():
    print('hi')
    bot = TgBot(TG_BOT_TOKEN)
    bot.run()
