from django.http.response import JsonResponse
from typing import Optional
from app.internal.services.user_service import TgUserStorage


def me_handler(request, username: Optional[str] = None) -> JsonResponse:
    storage = TgUserStorage()

    user = storage.get(username)

    if user is None:
        return JsonResponse({"message": "Not found"}, status=404)

    if user.phone_number in [None, ""]:
        return JsonResponse({"message": "Нет доступа, пожалуйста, установите номер телефона"}, status=403)

    return JsonResponse({"username": user.username, "phone_number": str(user.phone_number)}, status=200)
