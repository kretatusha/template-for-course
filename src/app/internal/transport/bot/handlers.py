from phonenumbers.phonenumberutil import NumberParseException

from app.internal.services.user_service import TgUserStorage


def start_handler(update, _):
    update.message.reply_text("Привет, пользователь!")
    storage = TgUserStorage()

    user = update.effective_user

    storage.update_or_create(username=user.username)


def set_phone_handler(update, _):
    storage = TgUserStorage()
    user = update.effective_user

    try:
        phone_number = update.message.text.split()[1]
    except IndexError:
        update.message.reply_text("Пустой ввод")

    try:
        storage.update_or_create(user.username, phone_number)
        update.message.reply_text("Успех")

    except NumberParseException:
        update.message.reply_text("Неправильный номер телефона, пожалуйста, попробуйте снова")


def _is_valid_input(user):
    return user is not None and user.phone_number not in [None, ""]


def me_handler(update, _):
    update.message.reply_text
    storage = TgUserStorage()

    user = storage.get(update.effective_user.username)

    if _is_valid_input(user):
        update.message.reply_text(f"FYI:\n{user}")
    else:
        update.message.reply_text("Нет доступа, пожалуйста, установите номер телефона")
