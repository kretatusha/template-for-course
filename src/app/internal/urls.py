from django.urls import path

from .transport.rest import handlers

urlpatterns = [
    path('me', handlers.me_handler),
]
