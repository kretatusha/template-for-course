import re
from typing import Optional

from phonenumber_field.phonenumber import PhoneNumber

from app.internal.models.tg_user import TgUser


class TgUserStorage:
    def update_or_create(self, username: Optional[str] = None, phone_number: Optional[str] = None) -> None:
        defaults = {}

        if username is not None:
            defaults["username"] = username

        if phone_number is not None:
            PhoneNumber.from_string(phone_number).is_valid()
            defaults["phone_number"] = phone_number

        TgUser.objects.update_or_create(username=username, defaults=defaults)

    def get(self, username: Optional[str] = None) -> Optional[TgUser]:
        try:
            return TgUser.objects.get(username=username)
        except TgUser.DoesNotExist:
            return None
